*Nom:* Alejandro Sotomayor

*M08:* Serveis de Xarxa i Internet

= Pràctica 3
= [underline]#Servidor DNS# (Primera Part)


=== Apartat 1: Instal·lació del servidor DNS a Zoidberg


.Captura 
==========================
image::images/Screenshot_1.jpg[]
image::images/Screenshot_2.jpg[]

Captura del resultat de la consulta DNS.

==========================

* *Quina és la IP de louvre.fr? Quina és la IP del servidor DNS que ens ha
respost?*


.Captura 
==========================
image::images/Screenshot_3.jpg[]
image::images/Screenshot_4.jpg[]

Captura de la instrucció que instal·la el servidor DNS.

==========================

.Captura 
==========================
image::images/Screenshot_5.jpg[]

Captura del resultat de la instrucció *netstat* que mostra el servidor DNS
funcionant.
==========================

.Captura 
==========================
image::images/Screenshot_6.jpg[]

Captura de la execució de *systemd-resolve --status*.

==========================

.Captura 
==========================
image::images/Screenshot_7.jpg[]

Captura de la instrucció utilitzada.

==========================

.Captura 
==========================
image::images/Screenshot_8.jpg[]

Captura de */etc/default/named*.

==========================

.Captura 
==========================
image::images/Screenshot_9.jpg[]

Captura de */etc/bind/named.conf.options*.

==========================

.Captura 
==========================
image::images/Screenshot_10.jpg[]

Captura de la instrucció utilitzada i el seu resultat.

==========================

.Captura 
==========================
image::images/Screenshot_15.jpg[]

Part del registre del *bind* corresponent a l’última petició..

==========================

.Captura 
==========================
image::images/Screenshot_11.jpg[]
image::images/Screenshot_13.jpg[]

Instrucció per bolcar el cau a un fitxer i contingut obtingut..

==========================

.Captura 
==========================
image::images/Screenshot_16.jpg[]

Instrucció per desactivar la depuració del servidor DNS.

==========================


= [underline]#Servidor DNS# (Segona Part)

=== Apartat 2: Configuració d’un servidor DNS només cache

.Captura 
==========================
image::images/Screenshot_17.jpg[]

Captura de les modificacions fetes a la configuracio.

==========================


=== Apartat 3: Configuració d’un servidor DNS _forwarding_


.Captura 
==========================
image::images/Screenshot_18.jpg[]

Captura de les modificacions fetes a la configuracio.

==========================

.Captura 
==========================
image::images/Screenshot_19.jpg[]

Captura de les modificacions fetes a la configuracio.

==========================

.Captura 
==========================
image::images/Screenshot_20.jpg[]

forwarders

==========================

.Captura 
==========================
image::images/Screenshot_21.jpg[]

==========================

.Captura 
==========================
image::images/Screenshot_22.jpg[]

==========================

.Captura 
==========================
image::images/Screenshot_23.jpg[]

==========================


= [underline]#Servidor DNS# (Tercera Part)

=== Apartat 4: Configuració dels clients


.Captura 
==========================
image::images/Screenshot_24.jpg[]

* Zoiberg

==========================

.Captura 
==========================
image::images/Screenshot_25.jpg[]

* Wong

==========================

.Captura 
==========================
image::images/Screenshot_26.jpg[]

* Client1

==========================

.Captura 
==========================
image::images/Screenshot_29.jpg[]

Comprovacio a Zoiberg

==========================

.Captura 
==========================
image::images/Screenshot_27.jpg[]

Comprovacio al Client1

==========================


=== Apartat 5: Creacio de la zona interna


.Captura 
==========================
image::images/Screenshot_32.jpg[]

En aquest fitxer es on configurarem la zona directa i inversa 
*/etc/bind/named.conf.local*

==========================


=== Apartat 6: Configuracio de la resolucio directa


.Captura 
==========================
image::images/Screenshot_33.jpg[]

Configuracio de la resolucio(zona) directa.

==========================


=== Apartat 7: Configuracio de la resolucio inversaa


.Captura 
==========================
image::images/Screenshot_34.jpg[]

Configuracio de la resolucio(zona) inversa.

==========================


=== Apartat 8: Comprovacio de la configuracio


.Captura 
==========================
image::images/Screenshot_35.jpg[]

*named-checkconf*

==========================

.Captura 
==========================
image::images/Screenshot_36.jpg[]

*named-checkzone* zona directa

==========================

.Captura 
==========================
image::images/Screenshot_37.jpg[]

*named-checkzone* zona inversa

==========================

.Captura 
==========================
image::images/Screenshot_43.jpg[]
image::images/Screenshot_44.jpg[]
image::images/Screenshot_45.jpg[]
image::images/Screenshot_46.jpg[]

Comprovacio resolucio directa des del clien1 amb la comanda *dig*

==========================

.Captura 
==========================
image::images/Screenshot_38.jpg[]
image::images/Screenshot_39.jpg[]
image::images/Screenshot_40.jpg[]
image::images/Screenshot_41.jpg[]

Comprovacio resolucio inversa des del clien1 amb la comanda *dig*

==========================