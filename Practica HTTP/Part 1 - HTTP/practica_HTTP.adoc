*Nom:* Alejandro Sotomayor

*M08:* Serveis de Xarxa i Internet

= Pràctica 6
= [underline]#Servidor HTTP - Part 1#

== Preparatius

=== Apartat 1: Instal·lació de l’Apache a Hermes


.Captura 
==========================
image::images/Screenshot_1.jpg[]

Instal·lació de l’apache2

==========================

.Captura 
==========================
image::images/Screenshot_5.jpg[]

Comprovació de connexió des de la xarxa local

==========================

.Captura 
==========================
image::images/Screenshot_2.jpg[]

Ports oberts amb la instal·lació d’Apache2

==========================

.Captura 
==========================
image::images/Screenshot_3.jpg[]

Estat d’execució

==========================

.Captura 
==========================
image::images/Screenshot_4.jpg[]

Logs inicials d’Apache2

==========================



=== Apartat 2: Creació de hosts virtuals



.Captura 
==========================
image::images/Screenshot_6.jpg[]

Captura del fitxer /etc/bind/named.conf.local.

==========================

.Captura 
==========================
image::images/Screenshot_12.jpg[]

Captura del fitxer de la zona.

==========================

.Captura 
==========================
image::images/Screenshot_7.jpg[]

image::images/Screenshot_8.jpg[]

image::images/Screenshot_20.jpg[]

Comprovació que la nova zona funciona correctament.

==========================

.Captura 
==========================
image::images/Screenshot_9.jpg[]

Sortida de ls -R on es vegi l’estructura de fitxers

==========================

.Captura 
==========================
image::images/Screenshot_10.jpg[]

Cadascun dels dos fitxers index.html.

==========================

.Captura 
==========================
image::images/Screenshot_11.jpg[]

Cadascun dels dos fitxers de configuració creats.

==========================

.Captura 
==========================
image::images/Screenshot_13.jpg[]

Instruccions utilitzades per activar i desactivar els hosts virtuals.

==========================

.Captura 
==========================
image::images/Screenshot_14.jpg[]

Sortida de l’ordre ls on es vegin els sites actius a sites-enabled.

==========================

.Captura 
==========================
image::images/Screenshot_15.jpg[]

image::images/Screenshot_16.jpg[]


Captura del navegador connectat a cadascun dels dos sites.

==========================


=== Apartat 3: Configuració del primer host virtual


.Captura 
==========================
image::images/Screenshot_17.jpg[]

Canvis fets a la configuració.

==========================

.Captura 
==========================
image::images/Screenshot_18.jpg[]

Canvis fets a la configuració.

==========================

.Captura 
==========================
image::images/Screenshot_19.jpg[]

Canvis fets a la configuració.

==========================

.Captura 
==========================
image::images/Screenshot_21.jpg[]

Canvis fets a la configuració.

==========================

.Captura 
==========================
image::images/Screenshot_22.jpg[]

Canvis fets a la configuració.

==========================

.Captura 
==========================
image::images/Screenshot_23.jpg[]

Canvis fets a la configuració.

==========================

.Captura 
==========================
image::images/Screenshot_24.jpg[]

Canvis fets a la configuració.

==========================

.Captura 
==========================
image::images/Screenshot_25.jpg[]

Canvis fets a la configuració.

==========================

.Captura 
==========================
image::images/Screenshot_26.jpg[]

image::images/Screenshot_27.jpg[]

Comprovació que funciona.

==========================
